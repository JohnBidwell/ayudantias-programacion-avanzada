#include <iostream>
#include <string>
using namespace std;

class Hogar{
private:
  string apellido;
  int ingreso;
  int numero_habitantes;
  string estado_civil;
public:
  Hogar(string apellido, int ingreso, int numero_habitantes, string estado_civil){
    this->apellido=apellido;
    this->ingreso=ingreso;
    this->numero_habitantes=numero_habitantes;
    this->estado_civil=estado_civil;
  }

  string getApellido(){
    return apellido;
  }
  int getIngreso(){
    return ingreso;
  }
  int getNumeroHabitantes(){
    return numero_habitantes;
  }
  string getEstadoCivil(){
    return estado_civil;
  }

  void setApellido(string new_apellido){
    apellido = new_apellido;
  }
  void setInreso(int new_ingreso){
    ingreso = new_ingreso;
  }
  void setNumeroHabitantes(int new_numero){
    numero_habitantes = new_numero;
  }
  void setEstadoCivil(string new_estado){
    estado_civil = new_estado;
  }
};

class Censo{
private:
  Hogar *hogares[1000];
public:
  Censo(){
    for(int i=0; i<1000; i++){
      hogares[i]=NULL;
    }
  }

  void ingresarHogar(Hogar *hogar){
    for(int i=0; i<1000; i++){
      if(hogares[i]==NULL){
        hogares[i]=hogar;
        break;
      }
      else if(i==99){
        cout << "Ya se han ingresado todos los hogares" << endl;
      }
    }
  }

  void buscarFamilia(string buscar){
    if(hogares[0]==NULL){
      cout << "No hay hogares ingresados" << endl;
    }
    else{
      for(int i=0; i<100; i++){
        if(hogares[i]==NULL){
          cout << "No hay hogares que coincidan con su búsqueda" << endl;
          break;
        }
        else if(hogares[i]->getApellido()==buscar){
          cout << "Familia: " << hogares[i]->getApellido() << endl;
          cout << "Ingreso familiar: " << hogares[i]->getIngreso() << endl;
          cout << "Numero de habitantes: " << hogares[i]->getNumeroHabitantes() << endl;
          cout << "Estado civil padres: " << hogares[i]->getEstadoCivil() << endl;
          break;
        }
        else if(i==99){
          cout << "No hay hogares que coincidan con su búsqueda" << endl;
        }
      }
    }
  }

  void menorIngreso(){
    int menor_ingreso = hogares[0]->getIngreso();
    int pos = 0;
    if(hogares[0]==NULL){
      cout << "No hay hogares ingresados" << endl;
    }
    else{
      for(int i=0; i<1000; i++){
        if(hogares[i]==NULL){
          break;
        }
        else if(hogares[i]->getIngreso() < menor_ingreso){
          menor_ingreso = hogares[i]->getIngreso();
          pos = i;
        }
      }
      cout << "Familia con menor ingreso: " <<  hogares[pos]->getApellido() << endl;
    }

  }

  void mayorIngreso(){
    int mayor_ingreso = hogares[0]->getIngreso();
    int pos = 0;
    if(hogares[0]==NULL){
      cout << "No hay hogares ingresados" << endl;
    }
    else{
      for(int i=0; i<1000; i++){
        if(hogares[i]==NULL){
          break;
        }
        else if(hogares[i]->getIngreso() > mayor_ingreso){
          mayor_ingreso = hogares[i]->getIngreso();
          pos = i;
        }
      }
      cout << "Familia con mayor ingreso: " <<  hogares[pos]->getApellido() << endl;
    }
  }

  void imprimirHogares(){
    if(hogares[0]==NULL){
      cout << "No hay hogares ingresados" << endl;
    }
    else{
      for(int i=0; i<100; i++){
        if(hogares[i]==NULL){
          break;
        }
        else{
          cout << "Familia: " << hogares[i]->getApellido() << endl;
          cout << "Ingreso familiar: " << hogares[i]->getIngreso() << endl;
          cout << "Numero de habitantes: " << hogares[i]->getNumeroHabitantes() << endl;
          cout << "Estado civil padres: " << hogares[i]->getEstadoCivil() << endl;
        }
      }
    }
  }

  void promedioHabitantesPorHogar(){
    if(hogares[0]==NULL){
      cout << "No hay hogares ingresados" << endl;
    }
    else{
      int total = 0;
      int cantidad_hogares = 0;
      for(int i=0; i<100; i++){
        if(hogares[i]==NULL){
          break;
        }
        else{
          total += hogares[i]->getNumeroHabitantes();
          cantidad_hogares += 1;
        }
      }
      cout << "Promedio habitantes por hogar: "<< total/cantidad_hogares << endl;
    }
  }

  void cantidadHabitantes(){
    if(hogares[0]==NULL){
      cout << "No hay hogares ingresados" << endl;
    }
    else{
      int total = 0;
      for(int i=0; i<100; i++){
        if(hogares[i]==NULL){
          break;
        }
        else{
          total += hogares[i]->getNumeroHabitantes();
        }
      }
      cout << "Habitantes: " << total << endl;
    }
  }


};

int main(){

  Hogar *h1= new Hogar("Stark", 200, 3, "Fallecidos");
  Hogar *h2= new Hogar("Targaryen", 100, 1, "Fallecidos");
  Hogar *h3= new Hogar("Simpsons", 1200, 5, "Casados");
  Hogar *h4= new Hogar("Flanders", 2000, 3, "Viudo");

  Censo *censo = new Censo();

  censo->imprimirHogares();

  censo->ingresarHogar(h1);
  censo->ingresarHogar(h2);
  censo->ingresarHogar(h3);
  censo->ingresarHogar(h4);

  censo->imprimirHogares();

  cout << endl;
  cout << "Buscar familia" << endl;
  censo->buscarFamilia("Targaryen");
  cout << endl;
  censo->buscarFamilia("Lannisters");
  cout << endl;

  censo->menorIngreso();
  censo->mayorIngreso();

  censo->promedioHabitantesPorHogar();

  censo->cantidadHabitantes();

}
