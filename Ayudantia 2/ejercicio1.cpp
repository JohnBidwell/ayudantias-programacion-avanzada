#include <iostream>
#include <string>
using namespace std;

class Serie{
private:
  string titulo;
  int annio_lanzamiento;
  int temporadas;
  int duracion_episodio;
  int numero_episodios;
public:
  Serie(string titulo, int annio_lanzamiento, int temporadas, int numero_episodios, int duracion_episodio){
    this->titulo=titulo;
    this->annio_lanzamiento=annio_lanzamiento;
    this->duracion_episodio=duracion_episodio;
    this->numero_episodios=numero_episodios;
    this->temporadas=1;
  }

  string getTitulo(){
    return titulo;
  }
  int getAnnioLanzamiento(){
    return annio_lanzamiento;
  }
  int getTemporadas(){
    return temporadas;
  }
  int getDuracionEpisodio(){
    return duracion_episodio;
  }
  int getNumeroEpisodios(){
    return numero_episodios;
  }

  void setTitulo(string new_titulo){
    titulo=new_titulo;
  }
  void setTemporadas(int new_temporadas){
    if(new_temporadas>temporadas){
      temporadas=new_temporadas;
    }
    else{
      cout << "El numero de temporadas debe ser mayor al actual" << endl;
    }
  }
  void setAnnioLanzamiento( int new_annio_lanzamiento){
    annio_lanzamiento = new_annio_lanzamiento;
  }
  void setDuracionEpisodio(int new_duracion_episodio){
    duracion_episodio=new_duracion_episodio;
  }
  void setNumeroEpisodios(int new_numero_episodios){
    numero_episodios=new_numero_episodios;
  }

  int duracionSerie(){
    return duracion_episodio*numero_episodios*temporadas;
  }
};

class Notflix{
private:
  Serie *series[10];
public:
  Notflix(){
    for(int i=0; i<10; i++){
      series[i]=NULL;
    }
  }

  void ingresarSerie(Serie *serie){
    for(int i=0; i<10; i++){
      if(series[i]==serie){
        cout << "La serie ya fue ingresada" << endl;
        break;
      }
      else if(series[i]==NULL){
        series[i]=serie;
        cout << "Serie ingresada satisfactoriamente" << endl;
        break;
      }
      else if(i==9){
        cout << "El catalogo ya contiene suficientes series" << endl;
      }
    }
  }

  void serieConMayorDuracion(){
    int indice_mayor=0;
    int duracion_mayor=-1;
    for(int i=0; i<10; i++){
      if (series[i]->duracionSerie() > duracion_mayor){
        duracion_mayor = series[i]->duracionSerie();
        indice_mayor=i;
      }
    }
    cout << "La serie con mayor duracion es: " << series[indice_mayor]->getTitulo() << " con " << series[indice_mayor]->duracionSerie() << endl;
  }

  void imprimirCatalogo(){
    if(series[0]!=NULL){
      for(int i=0; i<10; i++){
        cout << series[i]->getTitulo() << ", " << series[i]->getAnnioLanzamiento() <<
        " " << series[i]->getNumeroEpisodios() << " episodios, " << series[i]->getDuracionEpisodio()
        << "min., " << series[i]->getTemporadas() << " temporadas." << endl;
      cout << endl;
      }
    }
    else{
      cout << "Catalogo vacio" << endl;
      cout << endl;
    }
  }

  void modificarSerie(){
    int modificar, new_temporadas;
    cout << "Numero de serie a modificar(0-9): ";
    cin >> modificar;
    cout << series[modificar]->getTitulo() << ", " << series[modificar]->getAnnioLanzamiento() <<
    " " << series[modificar]->getNumeroEpisodios() << " episodios, " << series[modificar]->getDuracionEpisodio()
    << "min., " << series[modificar]->getTemporadas() << " temporadas." << endl;
    cout << "Nuevo numero temporadas: ";
    cin >> new_temporadas;
    series[modificar]->setTemporadas(new_temporadas);
    cout << endl;
  }
};

int main(){

  Notflix *catalogo = new Notflix();

  Serie *serie1 = new Serie("Mr. Robot", 2015, 2, 12, 45);
  Serie *serie2 = new Serie("Game of Thrones", 2011, 6, 10, 50);
  Serie *serie3 = new Serie("House of cards", 2013, 4, 10, 50);
  Serie *serie4 = new Serie("Pokemon", 1998, 1, 82, 22);

  catalogo->imprimirCatalogo();
  catalogo->ingresarSerie(serie1);
  catalogo->ingresarSerie(serie2);
  catalogo->ingresarSerie(serie3);
  catalogo->ingresarSerie(serie4);

  catalogo->modificarSerie();
  catalogo->imprimirCatalogo();
  catalogo->serieConMayorDuracion();

}
