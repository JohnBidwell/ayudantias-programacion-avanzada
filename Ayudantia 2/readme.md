## Ayudantía 2 programación avanzada

#### Objetivos
  * Trabajar arreglos de objetos

#### Arreglo de objetos

Un arreglo de objetos es un arreglo que almacena una cantidad de objetos del mismo tipo, donde cada indice del arreglo tiene un objeto con todos los métodos y atributos relativos a dicho objeto.

#### Declaración arreglos de objetos

Se declaran de la misma manera que declaramos un arreglo de enteros, strings, etc. Pero en este caso el tipo de dato que contendrá el arreglo es el nombre de la clase de objetos que contendrá el arreglo.

Al nombre del arreglo, al tratarse de objetos, se le debe anteponer un `*`.

Ejemplo:

`NombreClase *nombreArreglo[tamaño];`

#### Ingreso de objetos al arreglo

Existen dos formas de asignar objetos a cada una de las casillas del arreglo:

* Por asignación: Creando el objeto y asignándolo en la misma línea al arreglo.
`arreglo[i] = new NombreClase(<atributos>);`

* Por valor: Al tener un objeto asignado a una variable, este se asigna como valor a la casilla.
```
NombreClase *objeto = new NombreClase(<atributos>);
arreglo[i] = objeto;
```

#### Invocar métodos

Para invocar los métodos de un objeto almacenado en un arreglo, lo hacemos de la misma forma que si no estuviese almacenado en el, salvo que ahora debemos indicar la posición del arreglo en que está el objeto que queremos manipular.

`arreglo[i]->metodo();`


#### Ejercicio 1

Una nueva compañia llamada Notflix desea implementar en su servicio un catálogo de inicialmente 10 series, para esto solicita a un ingeniero la creación de esta plataforma, la cual debe mostrar al usuario el catálogo de series disponibles. Para esto se solicita:
* Implementar la clase Serie con todos sus métodos get() y set(), donde cada serie cuenta con un título, año de lanzamiento, duración de cada episodio (en minutos), cantidad de episodios y número de temporadas. Además un método que calcule la duración total de la serie (Duración de episodios \* número de episodios \* número de temporadas) en minutos.
* Crear clase Notflix, la cual se compone de un arreglo de 10 series. Esta clase tiene métodos para ingresar series al catálogo, ver la serie con mayor duración, imprimir el catálogo y modificar el numero de temporadas de las series.
* Ingresar series al catálogo.
* Imprimir el catálogo de series mostrando todos sus atributos.
* Modificar el número de temporadas de algunas series (la cantidad que quiera).
* Volver a imprimir el catálogo.
* Indicar la serie con mayor duración dentro del catálogo de Notflix.

#### Ejercicio 2

El gobierno de Chile ha decidido realizar un censo express a 1000 hogares, para esto debemos implementar la clase Hogar, que tiene de atributos: apellido paterno y/o materno, ingreso familiar, número de habitantes y el estado civil de los padres. Esta clase debe poseer todos los métodos get y set correspondientes.

Además, debemos implementar la clase Censo, la cual se compone de un arreglo de tamaño 1000 que albergará los hogares censados. Debe inicializar el arreglo vacio y debe poseer métodos para: buscar una familia en el arreglo utilizando un apellido ingresado por el usuario, determinar la familia con el menor ingreso, la familia con el mayor ingreso, listar todos los hogares censados, obtener la cantidad promedio de habitantes por hogar y la cantidad total de habitantes entre todos los hogares censados.

Dentro de la función main debe censar hogares y posteriormente utilizar todos los métodos de la clase Censo
