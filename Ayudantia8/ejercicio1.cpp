#include <iostream>
#include <map>
#include <vector>
#include <utility>
using namespace std;

class Concierto{
private:
  string ciudad;
  int entradasTotales;
  int disponibles;
  int valorEntrada;
public:
  Concierto(string ciudad, int entradasTotales, int valorEntrada){
    this->ciudad=ciudad;
    this->entradasTotales=entradasTotales;
    this->disponibles=disponibles;
    this->valorEntrada=valorEntrada;
  }
  string getCiudad(){
    return ciudad;
  }
  int getEntradas(){
    return entradasTotales;
  }/*
  int getDisponibles(){
    return disponibles;
  }*/
  int getValorEntrada(){
    return valorEntrada;
  }
};

class Gira{
private:
  map<string, Concierto*> gira;
public:
  void agregarConcierto(Concierto *concierto){
    gira[concierto->getCiudad()]=concierto;
  }
  Concierto menor_publico(){
    map<string, Concierto*>::iterator iterador;
    Concierto *menor;
    int cantidad;
    for(iterador = gira.begin(); iterador!=gira.end(); iterador++){
      if(iterador==gira.begin()){
          menor=gira[iterador->first];
          cantidad=gira[iterador->first]->getEntradas();
      }
      else{
          if (gira[iterador->first]->getEntradas() < cantidad){
              cantidad = gira[iterador->first]->getEntradas();
              menor = gira[iterador->first];
          }
      }
    }

    return *menor;

  }

  Concierto mayor_publico(){
    map<string, Concierto*>::iterator iterador;
    Concierto *mayor;
    int cantidad;
    for(iterador = gira.begin(); iterador!=gira.end(); iterador++){
      if(iterador==gira.begin()){
          mayor=gira[iterador->first];
          cantidad=gira[iterador->first]->getEntradas();
      }
      else{
          if (gira[iterador->first]->getEntradas() > cantidad){
              cantidad = gira[iterador->first]->getEntradas();
              mayor = gira[iterador->first];
          }
      }
    }

    return *mayor;

  }

};

int main(){
  Concierto *c1=new Concierto("Santiago", 45000,  25000);
  Concierto *c2=new Concierto("Vina del Mar", 25000 , 20000 );
  Concierto *c3=new Concierto("Rancagua", 15000 , 22000);
  Concierto *c4=new Concierto("Valdivia", 20000 , 15000);
  Concierto *c5=new Concierto("Concepcion", 30000 , 10000);
  Gira giraBanda;
  giraBanda.agregarConcierto(c1);
  giraBanda.agregarConcierto(c2);
  giraBanda.agregarConcierto(c3);
  giraBanda.agregarConcierto(c4);
  giraBanda.agregarConcierto(c5);
  cout << "Concierto con menor cantidad de publico: " <<
    giraBanda.menor_publico().getCiudad() << endl;
  cout << "Concierto con mayor cantidad de publico: " <<
    giraBanda.mayor_publico().getCiudad() << endl;
  return 0;
}
