#include <iostream>
using namespace std;

class Celular{
protected:
  string dueno;
  int numero;
public:
  Celular(string dueno, int numero){
    this->dueno=dueno;
    this->numero=numero;
  }
  string getDueno(){
    return dueno;
  }
  int getNumero(){
    return numero;
  }
  void setNombre(string newDueno){
    dueno=newDueno;
  }
  void setNumero(int newNumero){
    numero=newNumero;
  }
  virtual void imprimir()=0;
};

class iPhone:public Celular{
private:
  string appleID;
  string modelo;
public:
  iPhone(string appleID, string modelo, string dueno, int numero):Celular(dueno, numero){
    this->appleID=appleID;
    this->modelo=modelo;
  }
  string getAppleID(){
    return appleID;
  }
  string getModelo(){
    return modelo;
  }
  void setAppleID(string newAppleID){
    appleID=newAppleID;
  }
  void setModelo(string newModelo){
    modelo=newModelo;
  }
  void imprimir(){
    cout << dueno <<endl;
    cout << numero <<endl;
    cout << appleID <<endl;
    cout << modelo <<endl;
  } 
};
class Android: public Celular{
private:
  string googleID;
  string marca;
  string modelo;
public:
  Android(string googleID, string marca, string modelo, string dueno, int numero):Celular(dueno, numero){
    this->googleID=googleID;
    this->marca=marca;
    this->modelo=modelo;
  }
  void setGoogleID(string newGoogleID){
    googleID = newGoogleID;
  }
  void setMarca(string newMarca){
    marca=newMarca;
  }
  void setModelo(string newModelo){
    modelo=newModelo;
  }
  string getGoogleID(){
    return googleID;
  }
  string getMarca(){
    return marca;
  }
  string getModelo(){
    return modelo;
  }
  void imprimir(){
    cout << dueno <<endl;
    cout << numero <<endl;
    cout << googleID <<endl;
    cout << marca <<endl;
    cout << modelo <<endl;
  }
};
class Windows: public Celular{
private:
  string microsoftID;
  string marca;
  string modelo;
public:
  Windows(string microsoftID, string marca, string modelo, string dueno, int numero):Celular(dueno, numero){
    this->microsoftID=microsoftID;
    this->marca=marca;
    this->modelo=modelo;
  }
  void setMicrosoftID(string newMicrosoftID){
    microsoftID=newMicrosoftID;
  }
  void setMarca(string newMarca){
    marca=newMarca;
  }
  void setModelo(string newModelo){
    modelo=newModelo;
  }
  string getMicrosoftID(){
    return microsoftID;
  }
  string getModelo(){
    return modelo;
  }
  string getMarca(){
    return marca;
  }
  void imprimir(){
    cout << dueno <<endl;
    cout << numero <<endl;
    cout << microsoftID << endl;
    cout << marca <<endl;
    cout << modelo <<endl;
  }
};
int main(){

}
