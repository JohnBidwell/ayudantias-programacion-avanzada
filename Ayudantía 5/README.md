## Ayudantía 5 Programación avanzada

### Objetivos
  * Resolver problemas utilizando herencia y polimorfismo

### ¿Qué es la herencia?

  Es una propiedad que nos permite construir nuevas clases (hijas) a partir de clases existentes (padres), donde cada clase hija conservará las propiedades de la clase padre.

### Construcción de una clase hija

  * La clase padre debe contener atributos del tipo protected (no privados), los cuales solo derán accesibles dentro de la clase y las clases derivadas.

  * Definir el tipo de acceso de la clase hija:
      class <CLASE HIJA>: <tipo_acceso> <CLASE PADRE>

  * Definir el constructor de la clase hija:
      <CLASE HIJA>(atributos propios y del padre con su tipo):<CLASE PADRE>(Atributos del padre sin su tipo)

### Ejemplo

  ```
  class Persona{
  	protected:
  		string nombre;
  		string rut;
  	public:
  		Persona(string nombre, string rut){
  			this->nombre=nombre;
  			this->rut=rut;
  		}
  	// Metodos get() y set()
  };

  class Alumno:public Persona{
  	private:
  		string carrera;
  	public:
  		Alumno(string nombre, string rut, string carrera):Persona(nombre, rut){
  			this->carrera=carrera;
  		}
  	string getCarrera(){
  		return carrera;
  	}
  	// No es necesario definir los metodos get() y set() para nombre y rut, estos son heredados de la clase Persona.
  };

  int main(){
  	Alumno *a1 = new Alumno("John Bidwell", "18668515-6", "Ing Civil en Informatica y Telecomunicaciones");

  	cout << "Datos del alumno" << endl;
  	cout <<"========" << endl;
  	cout << "Nombre: " << a1->getNombre() << endl;
  	cout << "Rut: " << a1->getRut() << endl;
  	cout << "Carrera: " << a1->getCarrera() << endl;
  	cout << endl;
  }
  ```

### Funciones virtuales y Polimorfismo

  Polimorfismo es la propiedad de que a través de un método virtual (no implementado) un padre pueda permitir a sus hijos tener un método idéntico en nombre, pero con funcionamiento diferente para cada uno de ellos.

  * Funciones virtuales
  Una función virtual es una función que es declarada como 'virtual' en la clase padre y es redefinida en una o mas clases derivadas. Ademas, cada clase derivada puede tener su propia version de la función virtual.

  ```
    class Padre{
      virtual void funcion_virtual(){
        //Hace algo
      }
    class Hijo{
      void funcion_virtual(){
        // Hace otra cosa
      }
    }
  ```
  * Funciones virtuales puras

  Sirven para indicar que las clases derivadas deberán tener la implementación de la función virtual.
  ```
    class B {
      virtual void una_funcion_virtual_pura() = 0;
    };
  ```

### Ejercicio 1

Vivimos en un extraño mundo en el cual habitan diversas clases de Monstruos, los cuales se llaman Pokémon y Digimón. Estos monstruos poseen en común que ambos tienen un nombre y un tipo, y ejecutan acciones como hablar (los Pokémon dicen su nombre mientras que los Digimón pueden hablar el lenguaje humano) y pueden evolucionar (cambian su nombre).

Cada Pokémon posee además un nivel y poseen un entrenador o son salvajes.

Cada Digimón posee además una etapa (similar al nivel) y poseen un rol dentro del mundo (dato, vacuna o virus).

A usted se le pide programar (utilizando herencia) las clases Monstruo, Digimón y Pokémon, donde cada clase debe poseer todos sus métodos.

### Ejercicio 2

* Cree una clase Celular que contiene el nombre del dueño y su número de teléfono.
* Los Celulares se pueden clasificar en 3 tipos (Herencia): iPhone, Android y Windows Phone. (Atributo según clases heredadas). Donde el iPhone contiene un Apple ID y Modelo, los Android un Google ID, Marca y Modelo, y por último
los Windows Phone un Microsoft ID, Marca y Modelo.
* Cree una función que reciba un Celular e imprima los datos, ya sea iPhone, Android o Windows Phone.)
* Para demostrar que sus estructuras son correctas, su programa debe crear a lo menos 2 objetos de cada clase (iPhone, Android, Windows Phone) e imprimir en pantalla los atributos de estos.
* Para cada clase se debe crear su Constructor, métodos Get-Set, y un método Imprimir que muestra en pantalla los atributos de cada clase.
