#include <iostream>
#include <string>
using namespace std;

class Monster{
protected:
  string nombre;
  string tipo;
public:
  Monster(string nombre, string tipo){
    this->nombre=nombre;
    this->tipo=tipo;
  }
  string getNombre(){
    return nombre;
  }
  string getTipo(){
    return tipo;
  }
  void setNombre(string newNombre){
    nombre=newNombre;
  }
  void setTipo(string newTipo){
    tipo=newTipo;
  }

  void evolucionar(string newNombre){
    setNombre(newNombre);
  }

  virtual void hablar();

};

class Pokemon: public Monster{
private:
  int nivel;
  string entrenador;
public:
  Pokemon(string nombre, string tipo, int nivel, string entrenador):Monster(nombre, tipo){
      this->nivel=nivel;
      this->entrenador=entrenador;
  }
  void setNivel(){
    nivel += 1;
  }
  void setEntrenador(string newEntrenador){
    entrenador=newEntrenador;
  }
  int getNivel(){
    return nivel;
  }
  string getEntrenador(){
    return entrenador;
  }
  void hablar(){
    cout << nombre << endl;
  }
};

class Digimon:public Monster{
private:
  string etapa;
  string rol;
public:
  Digimon(string nombre, string tipo, string etapa, string rol):Monster(nombre, tipo){
    this->etapa=etapa;
    this->rol=rol;
  }
  void setEtapa(string newEtapa){
    etapa=newEtapa;
  }
  void setRol(string newRol){
    rol=newRol;
  }
  string getEtapa(){
    return etapa;
  }
  string getRol(){
    return rol;
  }

  void hablar(string frase){
    cout << frase << endl;
  }
  void hablar(){
    cout << "Hola mundo" << endl;
  }

};

int main(){
  Digimon *Agumon = new Digimon("Agumon", "Fuego", "Inicial", "Dato");
  Agumon->hablar();
return 0;
}
