#include <iostream>
using namespace std;

class Figura{
protected:
  float base;
  float altura;
public:
  Figura(float base, float altura){
    this->base=base;
    this->altura=altura;
  }
  void setBase(float nBase){
    base = nBase;
  }
  void setAltura(float nAltura){
    altura = nAltura;
  }
  float getBase(){
    return base;
  }
  float getAltura(){
    return altura;
  }

  virtual float area() = 0;
};

class Rectangulo: public Figura{
public:
  Rectangulo(float base, float altura):Figura(base, altura){}

  float area(){
    return base*altura;
  }
};

class Triangulo: public Figura{
public:
  Triangulo(float base, float altura):Figura(base, altura){}

  float area(){
    return (base*altura)/2;
  }
};
int main(){

  Triangulo *t1 = new Triangulo(4.0, 5.0);
  cout << "Area t1= " << t1->area() << endl;
  Triangulo *t2 = new Triangulo(8.2, 12.8);
  cout << "Area t2= " << t2->area() << endl;

  Rectangulo *r1 = new Rectangulo(4.0, 5.0);
  cout << "Area r1= " << r1->area() << endl;
  Rectangulo *r2 = new Rectangulo(8.2, 12.8);
  cout << "Area r2= " << r2->area() << endl;

  return 0;
}
