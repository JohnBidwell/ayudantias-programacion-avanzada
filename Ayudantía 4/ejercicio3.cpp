#include <iostream>
using namespace std;

class Publicacion{
protected:
  string autor;
  string nombre;
  int numero_paginas;
public:
  Publicacion(string autor, string nombre, int numero_paginas){
    this->autor=autor;
    this->nombre=nombre;
    this->numero_paginas=numero_paginas;
  }
  string getAutor(){
    return autor;
  }
  string getNombre(){
    return nombre;
  }
  int getNumeroPaginas(){
    return numero_paginas;
  }

};

class Libro: public Publicacion{
private:
  int costo;
public:
  Libro(string autor, string nombre, int numero_paginas, int costo):Publicacion(autor, nombre, numero_paginas){
    this->costo=costo;
  }
  int getCosto(){
    return costo;
  }
  void setCosto(int nCosto){
    costo = nCosto;
  }
};

class Paper: public Publicacion{
private:
  string congreso;
public:
  Paper(string autor, string nombre, int numero_paginas, string congreso):Publicacion(autor, nombre, numero_paginas){
    this->congreso=congreso;
  }
  string getCongreso(){
    return congreso;
  }
  void setCongreso(string nCongreso){
    congreso = nCongreso;
  }
};

int main(){
  return 0;
}
