#include <iostream>
using namespace std;

class Carta{
protected:
  string nombre;
  int costo;
public:
  Carta(string nombre, int costo){
    this->nombre=nombre;
    this->costo=costo;
  }
  void setNombre(string nNombre){
    nombre = nNombre;
  }
  void setCosto(int nCosto){
    costo = nCosto;
  }
  string getNombre(){
    return nombre;
  }
  int getCosto(){
    return costo;
  }

};

class Aliado: public Carta{
private:
  int ataque;
  string habilidad;
public:
  Aliado(string nombre, int costo, int ataque, string habilidad):Carta(nombre, costo){
    this->ataque=ataque;
    this->habilidad=habilidad;
  }
  void setHabilidad(string nHabilidad){
    habilidad = nHabilidad;
  }
  void setAtaque(int nAtaque){
    ataque = nAtaque;
  }
  string getHabilidad(){
    return habilidad;
  }
  int getAtaque(){
    return ataque;
  }
};

class Talisman: public Carta{
private:
  string habilidad;
public:
  Talisman(string nombre, int costo, string habilidad):Carta(nombre, costo){
    this->habilidad=habilidad;
  }
  void setHabilidad(string nHabilidad){
    habilidad = nHabilidad;
  }
  string getHabilidad(){
    return habilidad;
  }
};

int main(){
  return 0;
}
