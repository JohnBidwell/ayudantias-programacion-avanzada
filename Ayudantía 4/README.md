## Ayudantía 4 Programación avanzada

### Objetivos
  * Entender el concepto de herencia
  * Aplicar herencia en ejercicios con clases

### ¿Qué es la herencia?

Es una propiedad que nos permite construir nuevas clases (hijas) a partir de clases existentes (padres), donde cada clase hija conservará las propiedades de la clase padre.

### Construcción de una clase hija

* La clase padre debe contener atributos del tipo protected (no privados), los cuales solo derán accesibles dentro de la clase y las clases derivadas.

* Definir el tipo de acceso de la clase hija:
    class <CLASE HIJA>: <tipo_acceso> <CLASE PADRE>

* Definir el constructor de la clase hija:
    <CLASE HIJA>(atributos propios y del padre con su tipo):<CLASE PADRE>(Atributos del padre sin su tipo)

### Ejemplo

```
class Persona{
	protected:
		string nombre;
		string rut;
	public:
		Persona(string nombre, string rut){
			this->nombre=nombre;
			this->rut=rut;
		}
	// Metodos get() y set()
};

class Alumno:public Persona{
	private:
		string carrera;
	public:
		Alumno(string nombre, string rut, string carrera):Persona(nombre, rut){
			this->carrera=carrera;
		}
	string getCarrera(){
		return carrera;
	}
	// No es necesario definir los metodos get() y set() para nombre y rut, estos son heredados de la clase Persona.
};

int main(){
	Alumno *a1 = new Alumno("John Bidwell", "18668515-6", "Ing Civil en Informatica y Telecomunicaciones");

	cout << "Datos del alumno" << endl;
	cout <<"========" << endl;
	cout << "Nombre: " << a1->getNombre() << endl;
	cout << "Rut: " << a1->getRut() << endl;
	cout << "Carrera: " << a1->getCarrera() << endl;
	cout << endl;
}
```

### Funciones virtuales y Polimorfismo

Polimorfismo es la propiedad de que a través de un método virtual (no implementado) un padre pueda permitir a sus hijos tener un método idéntico en nombre, pero con funcionamiento diferente para cada uno de ellos.

  * Funciones virtuales
Una función virtual es una función que es declarada como 'virtual' en la clase padre y es redefinida en una o mas clases derivadas. Ademas, cada clase derivada puede tener su propia version de la función virtual.

```
  class Padre{
    virtual void funcion_virtual(){
      //Hace algo
    }
  class Hijo{
    void funcion_virtual(){
      // Hace otra cosa
    }
  }
```
  * Funciones virtuales puras

Sirven para indicar que las clases derivadas deberán tener la implementación de la función virtual.
```
  class B {
    virtual void una_funcion_virtual_pura() = 0;
  };
```



### Ejercicio 1

Diseña una clase denominada Figura, cuyos miembros dato sean: base y altura. Además del constructor y los métodos set (entregar los lados de la figura) y get(mostrar los lados de la figura), la clase contará con un método virtual que retorne el área de una figura. Figura contiene dos subclases denominadas Rectángulo y Triangulo. Defina estas dos subclases y declare dos objetos de tipo Triangulo y Rectángulo y visualice en pantalla el área de las figuras.  

### Ejercicio 2

Simularemos el juego de cartas Mitos y Leyendas. En este juego existen diferentes tipos de cartas (Aliado y Talisman), que poseen como atributo en común un costo y nombre.
Cada aliado posee un ataque y una habilidad. Los talismanes posee un habilidad.
Usted deberá:
* Implementar la clase Carta, son sus atributos y métodos get y set.
* Implementar la subClase Aliado, con sus atributos y métodos get y set.
* Implementar la subClase Talisman, con sus atributos y métodos get y set.
* Crar objetos aliados y talismanes

### Ejercicio 3

Dada una jerarquía de clases que representa distintos tipos de publicaciones, siendo la clase Publicación la raíz de la jerarquía, que contiene los campos autor, nombre y número de páginas. Cree las subclases Libros y Papers. En la clase Libros, se deberá tener la variable costo, y no se podrán crear subclases a partir de esta clase. En la clase Papers se deberá tener la variable congreso (STRING). Cree un constructor particular para la clase Publicacion, la cual reciba los campos correspondientes a su clase y en las otras dos clases cree el constructor correspondiente para que pueda llenar todas las variables además de los métodos get y set.
