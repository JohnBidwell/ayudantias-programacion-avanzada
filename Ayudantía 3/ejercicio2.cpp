#include <iostream>
#include <string>
using namespace std;

class Pokemon{
private:
  string nombre;
  int numero;
  int nivel;
  float ps;
  float pc;
  string ataque;
  float poder;
  float defensa;

public:
  Pokemon(string nombre, int numero, int nivel, int ps, int pc, string ataque, int poder, float defensa){
    this->nombre=nombre;
    this->numero=numero;
    this->nivel=nivel;
    this->ps=ps;
    this->pc=pc;
    this->defensa=defensa;
    this->ataque=ataque;
    this->poder=poder;
  }

  void setNivel(){
    nivel+=1;
  }

  string getNombre(){
    return nombre;
  }
  int getNumero(){
    return numero;
  }
  int getNivel(){
    return nivel;
  }

  int getPS(){
    return ps;
  }
  int getPC(){
    return pc;
  }
  string getAtaque(){
    return ataque;
  }
  int getPoder(){
    return poder;
  }

  void danoRecibido(Pokemon *pkmn){
    cout << pkmn->getNombre() << " ha realizado " << pkmn->getAtaque() << endl;
    float dano = 0.6*(((((0.5*float(pkmn->getNivel())+1.0)*pkmn->getPC()*pkmn->getPoder())/(25.0*defensa)))+2.0);
    cout << nombre << " ha recibido " << dano << " puntos de dano." << endl;
    descontarPS(dano); //Llamamos a esta funcion para que descuente el dano recibido a los PS del Pokemon
  }

  void descontarPS(float dano){
    ps -= dano;
    if(ps<0){ //ESto es porque los PS no pueden ser negativos
      ps = 0;
    }
    cout << "Salud de " << nombre << " " << ps << "PS." << endl;
    if(ps == 0){
      cout << nombre << " ha sido derrotado." << endl;
      cout << endl;
    }
  }
};

class Pokedex{
private:
  string entrenador;
  Pokemon *pokedex[151];
public:
  Pokedex(string entrenador){
    this->entrenador=entrenador;
    for(int i=0; i<151; i++){
      pokedex[i]=NULL;
    }
  }

  void registrarPokemon(Pokemon *pokemon){
    if(pokedex[pokemon->getNumero()-1]==NULL){
      pokedex[pokemon->getNumero()-1] = pokemon;
      cout << "Pokemon registrado en la Pokedex" << endl;
    }
    else{
      cout << "Este Pokemon ya ha sido registrado" << endl;
    }
  }

  void buscarPokemon(string buscar){
    for(int i=0; i<151; i++){
      if(pokedex[i]==NULL){
        continue;
      }
      if(pokedex[i]->getNombre()==buscar){
        cout <<  pokedex[i]->getNumero() << " " << pokedex[i]->getNombre() << " Nivel " << pokedex[i]->getNivel() << endl;
      }
      else if(i==150){
        cout << "Pokemon no encontrado" << endl;
      }
    }
  }

  string getEntrenador(){
    return entrenador;
  }

  void imprimirPokedex(){
    cout << "Pokedex de " << entrenador << endl;
    for(int i=0; i<151; i++){
      if(pokedex[i]!=NULL){
        cout << i+1 << " " << pokedex[i]->getNombre() << " nivel " << pokedex[i]->getNivel() << endl;
      }
      else{
        cout << i+1 << " ???" << endl;
      }
    }
  }

};


int main(){

  Pokemon *inicial = new Pokemon("Charmander", 4, 5, 40, 30, "ascuas", 30, 20 );
  Pokedex *pokedex = new Pokedex("John");
  pokedex->registrarPokemon(inicial);
  Pokemon *p1 = new Pokemon("Pikachu", 25, 10, 50, 40, "Impactrueno", 50, 35);
  Pokemon *p2 = new Pokemon("Pidgey", 16, 8, 25, 25, "remolino", 10, 28 );
  Pokemon *p3 = new Pokemon("Caterpie", 10, 4, 20, 20, "Placaje", 10, 20);
  Pokemon *p4 = new Pokemon("Growlithe", 58 , 15, 60, 70, "Ascuas", 30,80 );
  Pokemon *p5 = new Pokemon("Ponyta",77 , 18, 70, 80, "Rueda de fuego", 40, 80);
  pokedex->registrarPokemon(p1);
  pokedex->registrarPokemon(p2);
  pokedex->registrarPokemon(p3);
  pokedex->registrarPokemon(p4);
  pokedex->registrarPokemon(p5);

  Pokemon *trainer1[6] = {inicial, p1, p2, p3, p4, p5};
  Pokemon *trainer2[6] = {p3 ,p2, inicial, p5, p4, p1};

  cout << endl;
  int pokemon1=0; //Contadores para saber que pokemon estamos utilizando
  int pokemon2=0; //Contadores para saber que pokemon estamos utilizando
  while(pokemon1<6 || pokemon2<6){ //Mientras estemos utilizando alguno de nuestros 6 Pokemon del equipo
    trainer2[pokemon2]->danoRecibido(trainer1[pokemon1]); //Al pokemon del trainer 2 lo ataca el pokemon del trainer 1
    if(trainer2[pokemon2]->getPS()==0) { //Comprobamos si el Pokemon que recibio dano esta debilitado o no
      pokemon2+=1; //Pasamos a utilizar el siguiente Pokemon de nuestro equipo
      continue;
    }
    trainer1[pokemon1]->danoRecibido(trainer2[pokemon2]); //Al pokemon del trainer 1 lo ataca el pokemon del trainer 2
    if(trainer1[pokemon1]->getPS()==0){ //Comprobamos si el Pokemon que recibio dano esta debilitado o no
      pokemon1+=1; //Pasamos a utilizar el siguiente Pokemon de nuestro equipo
      continue;
    }
  }


}
