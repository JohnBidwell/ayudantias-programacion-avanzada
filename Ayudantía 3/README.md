## Ayudantía 3 programación avanzada

#### Objetivos
  * Arreglos de objetos
  * Interacción entre objetos


### Ejercicio 1

Al cumplir 10 años podemos iniciar nuestro viaje Pokémon, para esto debemos acudir donde el profesor Oak para escoger un Pokémon inicial, el cual nos acompañará durante toda la vida, además de una Pokédex en la cuál registraremos los Pokémon. Durante nuestro viaje tenemos la misión de capturar los 151 Pokémon existentes y registrarlos en la Pokédex, para esto debemos:


* Crear la clase Pokemon, donde cada Pokémon posee un nombre, numero y nivel. Debe tener los métodos get y ser correspondientes (El nombre y número no se puede modificar).
* Crear la clase Pokédex, la cual se compone de un arreglo de 151 Pokémon y el nombre del entrenador, el arreglo debe iniciar vacio. El constructor de la clase solo debe recibir el nombre del entrenador. La clase además posee métodos para ingresar un Pokémon a la Pokédex, ver si se posee un pokemon al buscarlo por su nombre, imprimir la Pokédex en el formato "NúmeroPokémon NombrePokémon NivelPokémon" (en el caso que no se haya registrado el Pokémon, imprimir "NumeroPokémon ???") y conocer el nombre del entrenador. Al ingresar un pokemon, este debe quedar registrado en la casilla del arreglo que corresponde a su número.
* En la función main, debemos escoger un pokemon inicial y registrarlo en la Pokédex, buscar un pokemon, luego registrar 5 Pokémon más y finalmente imprimir la Pokédex.

### Ejercicio 2

Ahora cada Pokémon posee, además de sus atributos iniciales, un atributo que indica la vida del Pokémon (PS), puntos de combate (PC), defensa y 1 ataque, cada uno con una cantidad de daño.

Además, ahora tenemos la posibilidad de realizar batallas contra otros entrenadores utilizando nuestro equipo de 6 Pokémon.

Para esto debemos:
* En el main tener un arreglo el cual nos permita identificar los 6 Pokémon que pertenecen a nuestro equipo, además de un arreglo, también con 6 Pokémon, para el entrenador contrario.
* Modificar la clase Pokémon para incluir los nuevos atributos, además de sus métodos get y set correspondientes.
* Implementar en la clase Pokémon nuevos métodos para calcular el daño que realiza cada Pokémon al Pokémon rival y un método que permita descontar PS al Pokémon que recibe un ataque.
* Simular una batalla entre Pokémon, recuerde que cada entrenador parte con un Pokémon (el primero en nuestro equipo), cada uno realiza un ataque a la vez hasta que uno de los dos quede con 0PS, cuando un Pokémon no tenga PS, el entrenador debe utilizar el siguiente Pokémon en su equipo.

La fórmula que utilizaremos para calcular el daño que causa un ataque es:
daño = 0.6 x (((0.5 x N + 1)* PC * P)/(25 * D) + 2)
N= Nivel Pokémon que ataca
PC = PC Pokémon que ataca
P = Poder ataque
D = Defensa Pokémon
