#include <iostream>
#include <string>
using namespace std;

class Pokemon{
private:
  string nombre;
  int numero;
  int nivel;
public:
  Pokemon(string nombre, int numero, int nivel){
    this->nombre=nombre;
    this->numero=numero;
    this->nivel=nivel;
  }

  void setNivel(){
    nivel+=1;
  }

  string getNombre(){
    return nombre;
  }
  int getNumero(){
    return numero;
  }
  int getNivel(){
    return nivel;
  }
};

class Pokedex{
private:
  string entrenador;
  Pokemon *pokedex[151];
public:
  Pokedex(string entrenador){
    this->entrenador=entrenador;
    for(int i=0; i<151; i++){
      pokedex[i]=NULL;
    }
  }

  void registrarPokemon(Pokemon *pokemon){
    if(pokedex[pokemon->getNumero()-1]==NULL){
      pokedex[pokemon->getNumero()-1] = pokemon;
      cout << "Pokemon registrado en la Pokedex" << endl;
    }
    else{
      cout << "Este Pokemon ya ha sido registrado" << endl;
    }
  }

  void buscarPokemon(string buscar){
    for(int i=0; i<151; i++){
      if(pokedex[i]==NULL){
        continue;
      }
      if(pokedex[i]->getNombre()==buscar){
        cout <<  pokedex[i]->getNumero() << " " << pokedex[i]->getNombre() << " Nivel " << pokedex[i]->getNivel() << endl;
      }
      else if(i==150){
        cout << "Pokemon no encontrado" << endl;
      }
    }
  }

  string getEntrenador(){
    return entrenador;
  }

  void imprimirPokedex(){
    cout << "Pokedex de " << entrenador << endl;
    for(int i=0; i<151; i++){
      if(pokedex[i]!=NULL){
        cout << i+1 << " " << pokedex[i]->getNombre() << " nivel " << pokedex[i]->getNivel() << endl;
      }
      else{
        cout << i+1 << " ???" << endl;
      }
    }
  }

};


int main(){
  Pokemon *inicial = new Pokemon("Charmander", 4, 5);
  Pokedex *pokedex = new Pokedex("John");

  pokedex->registrarPokemon(inicial);
  pokedex->buscarPokemon("Charmander");
  pokedex->buscarPokemon("Pikachu");

  Pokemon *p1 = new Pokemon("Pikachu", 25, 10);
  Pokemon *p2 = new Pokemon("Pidgey", 16, 8 );
  Pokemon *p3 = new Pokemon("Caterpie", 10, 4);
  Pokemon *p4 = new Pokemon("Growlithe", 58 , 15 );
  Pokemon *p5 = new Pokemon("Ponyta",77 , 18 );

  pokedex->registrarPokemon(p1);
  pokedex->registrarPokemon(p2);
  pokedex->registrarPokemon(p3);
  pokedex->registrarPokemon(p4);
  pokedex->registrarPokemon(p5);

  pokedex->imprimirPokedex();
}
