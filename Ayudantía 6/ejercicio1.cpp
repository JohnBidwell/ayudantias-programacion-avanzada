#include <iostream>
#include <string>
#include <vector>
#include <queue>
using namespace std;

class Persona{
private:
  string nombre;
  int edad;
public:
  Persona(string nombre, int edad){
    this->nombre=nombre;
    this->edad=edad;
  }
  string getNombre(){
    return nombre;
  }
  int getEdad(){
    return edad;
  }
};

class Cine{
private:
  vector <Persona*> sala;
  queue <Persona*> cola;
public:
  void IngresarACola(Persona *p){ // Recibe un objeto del tipo persona
    cola.push(p); // Inserta al final de la cola una persona
  }
  void ingresarASala(){
    if(cola.empty()!=true){ // Verifica si la cola está vacia, vacia = true, no vacia = false
      if(sala.size()<60){ // Ve el tamaño de la objetos en la sala para ver si se pueden insertar objetos
        sala.push_back(cola.front()); // Ingresamos en la sala la primera persona de la cola
        cola.pop(); // Desencolamos la persona que acabamos que ingresar a la sala
      }
      else{
        cout << "Sala llena, debe esperar" << endl;
      }
    }
    else{
      cout << "No hay personas en la cola" << endl;
    }
  }

  void vaciarSala(){
    while(sala.empty()!=true){ // Mientras la sala no este vacia
      sala.pop_back(); // Eliminamos el ultimo elemento de la sala
    }
    if (sala.empty()==true){ // Si la sala esta vacia
      cout << "Sala vacia" << endl;
    }
  }
  void genteEsperando(){
    if(cola.size()==0){ // Si tamano de cola = 0
      cout << "no hay gente esperando para entrar" << endl;
    }
    else{
      cout << "Hay " << cola.size() << " personas en la cola" << endl;
    }
  }
  void salaLlena(){
    if(sala.size()==60){
      cout << "La sala está llena" << endl;
    }
    else{
      cout << "Hay " << sala.size() << " personas en la sala" << endl;
    }
  }

  void imprimirSala(){
    for(int i=0; i < sala.size(); i++){
      cout << "Nombre: " << sala.at(i)->getNombre() << " - Edad: " << sala.at(i)->getEdad() << endl;
    }
  }

};

int main(){
  Persona *p1 = new Persona("John Bidwell", 22);
  Persona *p2 = new Persona("Yagami", 18);
  Persona *p3 = new Persona("Mr. Robot", 25);
  Cine c1;
  c1.genteEsperando();
  c1.IngresarACola(p1);
  c1.IngresarACola(p2);
  c1.IngresarACola(p3);
  c1.genteEsperando();
  c1.vaciarSala();
  c1.salaLlena();
  c1.ingresarASala();
  c1.salaLlena();
  c1.ingresarASala();
  c1.genteEsperando();
  c1.ingresarASala();
  c1.ingresarASala();
  c1.salaLlena();
  c1.imprimirSala();
  c1.vaciarSala();


  return 0;
}
