## Ayudantía 6 Programación avanzada

### Objetivos
* Conocer conceptos de lista, stack y queue
* Conocer clase Vector, Stack y Queue
* Resolver ejercicios utilizando la clase Vector, Stack y Queue


### Listas

Es una estructura de Datos de tamaño indefinido que puede incrementar su tamaño dinamicamente de acuerdo a las necesidades que se tengan.

En C++ manejaremos las listas mediante la clase llamada `Vector`

#### Clase Vector

Es una clase de C++ que nos permite operar con arreglos unidimensionales, los cuales pueden comenzar con un tamaño definido o cero (sin declarar) y, además, posee métodos para facilitar su manipulación.

##### Declaración de un Vector

* Importamos la clase Vector mendiante `#include <vector>`.
* Declaración:
  * Vector de largo cero: Vector <tipo de dato> nombre.
  * Vector de largo N: Vector <tipo de dato> nombre(N).

##### Acceso a los datos de un vector
Se accede a los datos de un vector de la misma forma que accedemos a los datos de una arreglo mediante el uso de `[]` o utilizando el método `at(i)` (donde i es la posición que queremos acceder).
La diferencia entre estos dos métodos es que `[]` permite acceder a posiciones inexistentes, mientras que `at(i)` solo permite acceder a posiciones que estre dentro del rango 0<=i<N.

##### Como agregar datos a un vector

* Si el tamaño fue definido, agregamos exactamente igual que en un array, incluso, podemos agregar en posiciones por sobre el largo definido inicialmente, aumentado así el tamaño del vector.

* Si el tamaño está o no está definido, podemos usar el método `push_back(elemento)`, que añadirá un elemento al final del vector.

##### Métodos relevante de la clase Vector

* size() -> retorna el tamaño del vector
* empty() -> retorna true si el vector está vacio, false en caso contrario
* at(i) -> accede al elemento en la posición i
* front() -> retorna una referencia al primer elemento del vector
* back() -> retorna una referencia al último elemento del vector
* push_back() -> Agrega un elemento al final
* pop_back() -> elimina el último elemento del vector
* erase(i) -> elimina el elemento en la posición i, luego, reajusta el vector


### Stacks (pilas)

Es una lista ordenada o una estructura de datos en que el acceso a sus elementos es del tipo LIFO (last in first out - Último en entrar, primero en salir)

En c++ se manejan a través de una clase especial llamda `stack`.

De un stack siempre podremos conocer su tamaño y únicamente podremos acceder al tope de la pila, es decir, solo podremos acceder, ingresar o eliminar elementos en el tope.

#### Clase Stack

Es una clase que permite manipular arreglos unidimensionales del tipo `Stack`. Comienzan con un tamaño cero y posee métodos que facilitan su manipulación.

##### Declaración

* Debemos importar la clase Stack mediante `#include <stack>`.
* Al declararlo: `stack <tipo de dato> nombre;`

##### Métodos relevante

* size() -> Retorna el tamaño del stack
* empty() -> Retorna true si el stack está vacio
* push(dato) -> Agrega un elemento al tope del stack
* pop() -> Remueve el elemento del tope del stack
* top() -> Accede al elemento del tope del stack

####  Queue (Colas)

Una Cola es una lista ordenada o una estructura de datos en que el modo de acceso a sus elementos es del tipo FIFO (First in First out, primero en entrar primero en salir).

En c++ se manejan a través de una clase especial llamda `queue`.

De un stack siempre podremos conocer su tamaño y únicamente podremos ingresar un elemento en la entrada, eliminar un elemento en la salida y consultar los elementos que están en la entrada y salida.

##### Clase Queue

Es una clase que permite manipular arreglos unidimensionales del tipo `Queue`. Comienzan con un tamaño cero y posee métodos que facilitan su manipulación.

##### Declaración

* Debemos importar la clase Stack mediante `#include <queue>`.
* Al declararlo: `queue <tipo de dato> nombre;`

##### Métodos relevante

* size() -> Retorna el tamaño de la cola
* empty() -> Retorna true si la cola está vacio
* push(dato) -> Agrega un elemento en la entrada de la cola
* back() -> Retorna el elemento en la entrada de la cola
* pop() -> Remueve el elemento de salida de la cola
* front() -> Retorna elemento en la salida de la cola

### Ejercicio 1 (Listas)

### Ejercicio 2 (Pilas)

### Ejercicio 3 (Colas)

Una multitud de personas está esperando para ver el estreno de su película preferida, pero el cine al cual acudieron no tiene definido un sistema para asignar todas estas personas a laúnica sala que posee, por lo cuál se le ha encargado implementar un sistema que gestione el ingreso de personas a la sala, considerando que cada sala posee una capacidad máxima de 60 personas. Para esto debe:

* Implementar la clase Persona, donde cada persona tiene un nombre y edad.
* Implementar en la clase Cine, donde cada cine posee una lista con las personas que hay en la sala y una cola con las personas que están esperando, métodos para:
  * Ingresar personas a la cola y a la sala del cine
  * Vaciar la sala del cine
  * Verificar si hay gente esperando para ingresar a la sala
  * Ver si la sala está llena
