#include <iostream>
#include <string>
using namespace std;

class Serie{
private:
  string titulo;
  string director;
  string canal;
  int temporadas;
  string genero;
public:
  Serie(string titulo, string director, string canal, string genero){
    this->titulo= titulo;
    this->director=director;
    this->canal=canal;
    //this->temporadas=temporadas;
    this->genero=genero;
    this->temporadas=1;
  }

  string getTitulo(){
    return titulo;
  }
  string getDirector(){
    return director;
  }
  string getCanal(){
    return canal;
  }
  int getTemporadas(){
    return temporadas;
  }
  string getGenero(){
    return genero;
  }

  void setTitulo(string new_titulo){
    titulo=new_titulo;
  }
  void setCanal(string new_canal){
    canal=new_canal;
  }
  void setTemporadas(int new_temporadas){
    if(new_temporadas>temporadas){
      temporadas=new_temporadas;
    }
    else{
      cout << "El numero de temporadas debe ser mayor al actual" << endl;
    }
  }
};

int main(){

  Serie *got= new Serie("Game of thrones", "David Benioff", "HBO", "Fantasia medival, drama y aventura");
  cout << got->getTitulo() << " es una serie dirigida por " << got->getDirector() << ", de genero " << got->getGenero()
  << " y transmitida por " << got->getCanal() << ", la serie cuenta con " << got->getTemporadas() << " temporadas." << endl;

  cout << endl;
  int nuevo_temporadas;
  string nuevo_titulo;
  string nuevo_canal;

  cout << "Ingrese numero de temporadas: ";
  cin >> nuevo_temporadas;
  got->setTemporadas(nuevo_temporadas);

  cout << "Ingrese nuevo titulo: ";
  getline(cin, nuevo_titulo);
  got->setTitulo(nuevo_titulo);

  cout << "Ingrese nuevo canal de transmision: ";
  getline(cin, nuevo_canal);
  got->setCanal(nuevo_canal);

  cout << endl;

  cout << got->getTitulo() << " es una serie dirigida por " << got->getDirector() << ", de genero " << got->getGenero()
  << " y transmitida por " << got->getCanal() << ", la serie cuenta con " << got->getTemporadas() << " temporadas." << endl;

  return 0;
}
