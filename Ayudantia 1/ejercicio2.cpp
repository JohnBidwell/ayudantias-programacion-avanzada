#include <iostream>
#include <string>
using namespace std;

class Libro{
private:
  string titulo;
  string autor;
  int annio;
  int paginas;
public:
  Libro(string titulo, string autor, int annio,int paginas){
    this->titulo=titulo;
    this->autor=autor;
    this->annio=annio;
    this->paginas=paginas;
  }

  string getTitulo(){
    return titulo;
  }
  string getAutor(){
    return autor;
  }
  int getAnnio(){
    return annio;
  }
  int getPaginas(){
    return paginas;
  }

  void setTitulo(string new_titulo){
    titulo=new_titulo;
  }
  void setAutor(string new_autor){
    autor=new_autor;
  }
  void setAnno(int new_annio){
    annio=new_annio;
  }
  void setPaginas(int new_paginas){
    paginas=new_paginas;
  }

  void toString(){
    cout << "El libro " << getTitulo() << " creado por " << getAutor()
    << " y publicado en el annio " << getAnnio() << " posee " << getPaginas() << " paginas" << endl;
  }
};
int main(){
  Libro *l1 = new Libro("Game of Thornes", "George R.R. Martin", 1996, 694);
  Libro *l2 = new Libro("Danza de dragones", "George R.R. Martin", 2011, 1040);

  l1->toString();
  l2->toString();

  if(l1->getPaginas() > l2->getPaginas() ){
    cout << l1->getTitulo() << " posee mas paginas que " << l2->getTitulo() << endl;
  }
  else if(l2->getPaginas() > l1->getPaginas() ){
    cout << l2->getTitulo() << " posee mas paginas que " << l1->getTitulo() << endl;
  }
  else{
    cout << "Ambos poseen la misma cantidad de paginas" << endl;
  }
}
