Ayudantía 1 programación avanzada

* Objetivos
  * Programación de clases
  * Instancias
  * Casos en que se deba desprender información y programar  


### Implementación de una clase

```C++
class NombreClase{
  private:
    // Atributos

  public:
    // Contructor
    // Métodos
};
```

El nombre de una clase va con formato UpperCamelCase y se declaran usando la palabra reservada `class`.

Los atributos son las variables con las que trabajará una clase, ejemplo:
  * `class Persona` tendría los atributos nombre, edad, sexo, etc.

### Constructor

Toda clase debe definir un Constructor, que indica el estado inicial que tendrá cada objeto. Este debe ir en la seccion `public` de la clase, llevan el mismo nombre de la clase y no tienen dato de retorno.

### Métodos

Son 'funciones' que pueden o no realizar los objetos
* Se definen mediante lowerCamelCase
* "Invocan" acciones en los objetos
* Permiten acceder y/o modificar los datos privados del objeto
* Deben ser llamados a través de un objeto

### Sectores públicos y privados
Son espacios en los que se declaran aquellos datos y procedimientos a los que queremos controlar su acceso.
* Privados: Solo pueden ser accedidos desde dentro de la clase
* Públicos: Pueden ser accedidos desde fuera de la clase

### Creación de objetos

Crearemos un objeto anteponiendo `*` al nombre que tendrá el objeto, y lo declararemos de la siguente manera:

`NombreClase *nombreObjeto = new Constructor(parametros)`

### Uso de métodos

Invocaremos los métodos con el comando `->` de la siguente manera:

`nombreObjeto->metodo();`

### Programa de ejemplo

Creación de la clase persona con atributos nombre, edad, sexo.

### Ejercicio 1

Generar objetos del tipo Serie, donde cada serie posee un título, director, canal de transmisión, número de temporadas y género. El director y género no se puede modificar. Por defecto el número de temporadas es 1 y cuando el número de temporadas se modifica, solo puede aumentar. Luego:
* Implementar métodos get() y set().
* Crear un objeto del tipo Serie con sus atributos.
* Realizar la impresión de los datos.
* Modificar el número de temporadas, título e idioma y volver a imprimir sus datos.


### Ejercicio 2

Generar objetos del tipo Libro, donde cada libro posee un título, autor, año de publicación y número de páginas. Crear:
* Respectivos métodos get() y set() para cada atributo
* Método toString() que permita imprimir los datos de cada libro en el formato siguente:
  “El libro <su_titulo> creado por el autor <su_autor> y publicado en el año <año de publicación> tiene <num_paginas> páginas”
* En la función main() crear dos libros, imprimir sus datos e indicar cual es el que tiene más páginas.
