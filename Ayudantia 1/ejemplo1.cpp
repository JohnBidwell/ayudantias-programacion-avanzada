#include <iostream>
using namespace std;

class Persona{
private:
  string nombre;
  int edad;
  string sexo;
public:
  Persona(string nombre, int edad, string sexo){
    this->nombre = nombre;
    this->edad = edad;
    this->sexo = sexo;
  }

  string getNombre(){
    return nombre;
  }
  int getEdad(){
    return edad;
  }
  string getSexo(){
    return sexo;
  }

  void setNombre(string newNombre){
    nombre = newNombre;
  }
  void setEdad(){
    edad += 1;
  }
  void setSexo(){
    if(sexo=="M"){
      sexo = "F";
    }
    else{
      sexo = "M";
    }
  }
};

int main(){

  Persona *p1 = new Persona("John Bidwell", 22, "M");
  cout << "Nombre: " << p1->getNombre() << endl;
  cout << "Edad: " << p1->getEdad() << endl;
  cout << "Sexo: " << p1->getSexo() << endl;
  return 0;

}
