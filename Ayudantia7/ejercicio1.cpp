#include <iostream>
#include <string>
#include <map>
#include <utility>
using namespace std;

class Persona{
private:
  string nombre;
  int edad;
  int numero;
  string direccion;
public:
  Persona(string nombre, int edad, int numero, string direccion){
    this->nombre=nombre;
    this->edad=edad;
    this->numero=numero;
    this->direccion=direccion;
  }
  string getNombre(){
    return nombre;
  }
  int getNumero(){
    return numero;
  }
  int getEdad(){
    return edad;
  }
  string getDireccion(){
    return direccion;
  }
};

int main(){
  map<string, Persona*> agenda;

  Persona *p1 = new Persona("John", 22, 955555555, "The North");
  Persona *p2 = new Persona("Frodo", 17, 328749327, "La Comarca");
  Persona *p3 = new Persona("Jon", 16, 7348974932, "Winterfell");
  Persona *p4 = new Persona("Naruto", 15, 7391713, "Konoha");

  agenda["John"] = p1;
  agenda["Frodo"] = p2;
  agenda["Jon"] = p3;
  agenda["Naruto"] = p4;


  map<string, Persona*>::iterator iterador;

  for(iterador = agenda.begin(); iterador!=agenda.end(); iterador++){
    cout << "Nombre: " << agenda[iterador->first]->getNombre() << endl;
    cout << "Edad: " << agenda[iterador->first]->getEdad() << endl;
    cout << "Numero: " << agenda[iterador->first]->getNumero() << endl;
    cout << "Direccion: " << agenda[iterador->first]->getDireccion() << endl;
  }

  cout << "=== Busqueda de contacto ===" << endl;
  cout << endl;
  string buscar;
  cout << "Contacto a buscar: ";
  cin >> buscar;

  iterador = agenda.find(buscar);
  if(iterador!= agenda.end()){
    cout << "Nombre: " << agenda[iterador->first]->getNombre() << endl;
    cout << "Edad: " << agenda[iterador->first]->getEdad() << endl;
    cout << "Numero: " << agenda[iterador->first]->getNumero() << endl;
    cout << "Direccion: " << agenda[iterador->first]->getDireccion() << endl;
  }
  else{
    cout << "Contacto no existe" << endl;
  }
}
