## Ayudantía 7 Programación avanzada

### Objetivos
* Conocer la estructura diccionarios
* Conocer la clase asociada a estas estructuras que posee C++, Clase MAP
* Conocer la directiva que permite manipular, de mejor manera, los diccionarios ---- Utility
* Realizar un ejercicio utilizando diccionarios como herramienta de apoyo a la solución


### Diccionarios

Son una lista de consulta de términos los cuales tienen valores que pueden ser accedidos por medio de una llave. Estos valores no están ordenados.

En C++ se manipulan a través de la clase `map`

#### Propiedades de los diccionarios

* Las claves son únicas
* Un mismo valor se puede asignar a diferentes claves
* Los valores son reemplazables
* El acceso a la información es por medio de la llave, no hay forma de acceder por medio del valor
* Pueden contener cualquier tipo de datos (string, int, float, char, etc)

#### Clase map

Es una clase que nos permitirá operar estructuras del tipo diccionario. Comienzan con un tamaño cero (sin declarar), y, además, posee métodos que facilitan su manipulación.

##### Declaración de un diccionario

* Importar la clase map mediante `#include <map>`.
* Declaración: map <tipo de dato de la clave, tipo de dato de los valores> nombre;

#### Métodos clase map

* .begin() -> Hace referencia al primer elemento
* .end() -> Hace referencia al elemento pasado el último, o que ya no hay más elementos.
* .empty() -> Retorna true si hay 0 elementos, false en caso contrario
* .size() -> Número de elementos
* nombreMap[clave] -> Retorna el valor asociado a dicha clave
* .find(clave) -> Retorna un iterator en el caso que lo encuentre, en el caso contrario retorna un iterator a .end()

### Pair y plantilla Utility

Un `pair` es una plantilla que contendrá una pareja de valores (first y second). Cada uno de los miembros del pair pueden ser de diferente tipo.

Para utilizarla en nuestros programa, debemos incluir `#include <utility>`

#### ¿Para qué sirve en los diccionarios?

Nos permitirá acceder a los datos de un diccionario al poder generar un “iterador” o variable de acceso asociada a dicha estructura.

Ejemplo: Imprimir los datos de un diccionario

```
// Tenemos un map llamo myMap que contiene personas (con ID y nombre)
map<string, int>::iterator iterador;
for(iterador=myMap.begin(); iterador!=myMap.end(); iterador++){
  cout << "ID: " << myMap[iterador->first]->getID() << endl;
  cout << "Nombre: " << myMap[iterador->first]->getNombre() << endl;
}
```

### Ejercicio 1

Queremos crear un directorio de personas, donde cada persona contará con un nombre, número telefónico, edad y dirección. Estas personas deben ser almacenadas de tal forma que tan solo ingresando el nombre, se despliegue su número telefónico. Para esto debe:
* Crear la clase Persona
* En el main(), crear una cantidad de personas y guardarlos en la agenda, luego:
  * Imprimir todos los contactos que hay en la agenda.
  * Buscar un contacto, en caso que no exista se debe indicar que el contacto no existe, en caso que exista, imprimir su información.

### Ejercicio 2
